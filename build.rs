extern crate protobuf_build;

use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    println!("cargo:rerun-if-changed=proto/messages.proto");
    let root = PathBuf::from(env::var("CARGO_MANIFEST_DIR").unwrap());
    let source = root.join("proto");

    let out = root.join("src").join("proto");

    let mut compiler = protobuf_build::Compiler::new(&source, &out);

    compiler.compile("messages.proto").unwrap();
}
