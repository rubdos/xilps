extern crate mio;
extern crate byteorder;
extern crate protobuf;
extern crate netbuf;
extern crate rand;
extern crate time;
extern crate sdl2;
extern crate sdl2_ttf;
extern crate sdl2_gfx;

use std::collections::HashMap;
use std::time::Duration;

use mio::*;
use mio::tcp::TcpStream;

use sdl2::pixels::Color;
use sdl2::keyboard::Keycode;
use sdl2::rect::Rect;
use sdl2::render::TextureQuery;
use sdl2_gfx::primitives::DrawRenderer;

use field::{Field, FIELD_WIDTH, FIELD_HEIGHT, Cell};

pub mod message;
pub mod proto;
pub mod field;
pub mod contour;
use contour::Contour;
mod player;

use proto::messages::MessageType;

use message::BufferedMessageQueue;

use player::{Direction};

const SERVER: Token = Token(0);
const TIMER: Token = Token(1);

const SCREEN_WIDTH: u32 = 800;
const SCREEN_HEIGHT: u32 = 640;

const KOTTEKE_WIDTH: u32 = 32;
const KOTTEKE_HEIGHT: u32 = 32;
const KOTTEKES_H: i32 = (SCREEN_WIDTH / KOTTEKE_WIDTH) as i32;
const KOTTEKES_V: i32 = (SCREEN_HEIGHT / KOTTEKE_HEIGHT) as i32;

const PLAYER_RAD: u32 = 19;

const BACKGROUND_COLOR: Color = Color::RGBA(245, 245, 245, 255);
const EMPTY_KOTTEKE_COLOR: Color = Color::RGBA(217, 217, 217, 255);

enum GameClientState {
    Lobby,
    Playing,
}

struct Player {
    direction: Direction,
    name: String,
    id: usize,
    pub coords: (usize, usize),
    color: (u8, u8, u8),
    tail: Vec<(i32, i32)>,
}

struct GameClient {
    pub messenger: BufferedMessageQueue,
    pub state: GameClientState,
    pub name: String,
    ttf_context: sdl2_ttf::Sdl2TtfContext,
    font: sdl2_ttf::Font,
    field: Field,

    coords: (u32, u32),
    camera_location: (f64, f64),
    received_first_coords: bool,
    direction: Direction,

    players: HashMap<usize, Player>,
    timer: mio::timer::Timer<&'static str>,
}

macro_rules! rect(
    ($x:expr, $y:expr, $w:expr, $h:expr) => (
        Rect::new($x as i32, $y as i32, $w as u32, $h as u32)
    )
);

fn coords_from_proto(pro: &proto::messages::Coords) -> (u32, u32) {
    (pro.get_x(), pro.get_y())
}

fn get_centered_rect(rect_width: u32, rect_height: u32, cons_width: u32, cons_height: u32) -> Rect {
    let wr = rect_width as f32 / cons_width as f32;
    let hr = rect_height as f32 / cons_height as f32;

    let (w, h) = if wr > 1f32 || hr > 1f32 {
        if wr > hr {
            //println!("Scaling down! The text will look worse!");
            let h = (rect_height as f32 / wr) as i32;
            (cons_width as i32, h)
        } else {
            //println!("Scaling down! The text will look worse!");
            let w = (rect_width as f32 / hr) as i32;
            (w, cons_height as i32)
        }
    } else {
        (rect_width as i32, rect_height as i32)
    };

    let cx = (SCREEN_WIDTH as i32 - w) / 2;
    let cy = (SCREEN_HEIGHT as i32 - h) / 2;
    rect!(cx, cy, w, h)
}

impl GameClient {
    fn new(socket: TcpStream, poll: &Poll) -> GameClient {
        let ttf_context = sdl2_ttf::init().unwrap();
        let font_path = std::path::Path::new("Hack-Regular.ttf");
        let font = ttf_context.load_font(font_path, 32).unwrap();
        let field = Field::new(FIELD_WIDTH, FIELD_HEIGHT);

        // Create a timer
        // 100 times a second, 10 milliseconds
        let mut timer = mio::timer::Builder::default()
            .tick_duration(Duration::from_millis(10))
            .build();
        timer.set_timeout(Duration::from_millis(10), "UPDATE").unwrap();
        poll.register(&timer, TIMER, Ready::all(),
                      PollOpt::edge()).unwrap();
        GameClient {
            messenger: BufferedMessageQueue::new(socket),
            state: GameClientState::Lobby,
            name: String::new(),
            ttf_context: ttf_context,
            font: font,
            field: field,

            direction: Direction::Up, // Default state
            coords: (0,0),
            camera_location: (-100.,-100.),
            received_first_coords: false,

            players: HashMap::new(),
            timer: timer,
        }
    }

    fn register(&mut self) {
        let mut registration = proto::messages::RegisterPlayer::new();
        registration.set_name(self.name.clone());
        self.messenger.send_message(proto::messages::MessageType::REGISTER, &registration);
    }

    #[allow(unused_variables)]
    fn event(&mut self, event_loop: &Poll, event: Event) {
        match event.token() {
            TIMER => { // For animations, called 100 times a second
                let speed = (self.coords.0 as f64 - self.camera_location.0,
                             self.coords.1 as f64 - self.camera_location.1);
                // Calculate new camera location
                self.camera_location.0 += speed.0 * 0.01;
                self.camera_location.1 += speed.1 * 0.01;
                self.timer.set_timeout(Duration::from_millis(10), "UPDATE").unwrap();
            }
            SERVER => {
                // Received a message from the server
                self.messenger.run();

                while let Some(message_type) = self.messenger.next_message_type() {
                    match message_type {
                        MessageType::BLOCKUPDATE => {
                            let msg = self.messenger.pop_message::<proto::messages::BlockUpdate>().unwrap();
                            self.field.merge(&msg);
                        }
                        MessageType::PLAYER_STATUS => {
                            let msg = self.messenger.pop_message::<proto::messages::PlayerStatus>().unwrap();
                            let id = msg.get_id() as usize;
                            if let Some(mut player) = self.players.get_mut(&id) {
                                player.direction = Direction::from_proto(msg.get_direction());
                                let coords = coords_from_proto(msg.get_coords());
                                player.coords = (coords.0 as usize, coords.1 as usize);
                                if msg.has_tail() {
                                    let coords = coords_from_proto(msg.get_tail_begin());
                                    let coords = (coords.0 as usize, coords.1 as usize);
                                    player.tail = Contour::to_coordinates(coords, Vec::from(msg.get_tail()));
                                    for c in player.tail.iter() {
                                        self.field.get_mut_cell(c.0, c.1).unwrap().set_tail(player.id);
                                    }
                                }
                            } else {
                                // We received our own coordinates and direction.
                                self.direction = Direction::from_proto(msg.get_direction());
                                self.coords = coords_from_proto(msg.get_coords());
                                if ! self.received_first_coords && self.coords.0 != 0 && self.coords.1 != 0 {
                                    self.received_first_coords = true;
                                    self.camera_location.0 = self.coords.0 as f64;
                                    self.camera_location.1 = self.coords.1 as f64;
                                }
                                println!("New coords: ({}, {})", self.coords.0, self.coords.1);
                            }
                        }
                        MessageType::NEW_PLAYER => {
                            let msg = self.messenger.pop_message::<proto::messages::NewPlayer>().unwrap();
                            let status = msg.get_player_status();
                            self.players.insert(status.get_id() as usize,
                                Player {
                                    id: status.get_id() as usize,
                                    coords: (status.get_coords().get_x() as usize,
                                             status.get_coords().get_y() as usize),
                                    name: String::from(msg.get_name()),
                                    direction: Direction::from_proto(status.get_direction()),
                                    color: (msg.get_color().get_R() as u8,
                                            msg.get_color().get_G() as u8,
                                            msg.get_color().get_B() as u8),
                                    tail: Vec::new(),
                                });
                        }
                        MessageType::ROTATE |
                        MessageType::REGISTER => {
                            //Nobody sends these, for now unreachable, but we should probably catch these.
                            unreachable!();
                        }
                    }
                }
            }
            token => unreachable!()
        }
    }

    fn draw(&self, renderer: &mut sdl2::render::Renderer) {
        match self.state {
            GameClientState::Lobby => {
                self.draw_lobby(renderer);
            }
            GameClientState::Playing => {
                self.draw_playing(renderer);
            }
        }
    }

    fn text_input(&mut self, text: String) {
        self.name.push_str(&text);
        println!("Current name: {}", self.name);
    }

    fn key_down(&mut self, keycode: Keycode) {
        match self.state {
            GameClientState::Lobby => {
                match keycode {
                    Keycode::Return => {
                        self.register();
                        self.state = GameClientState::Playing;
                    }
                    Keycode::Backspace => {
                        self.name.pop();
                    }
                    _ => {} //nop
                };
            }
            GameClientState::Playing => {
                match keycode {
                    Keycode::Left |
                    Keycode::Right |
                    Keycode::Down |
                    Keycode::Up => {
                        self.change_direction(Direction::from_sdl(keycode));
                    }
                    _ => {} //nop
                };
            }
        }
    }

    fn change_direction(&mut self, dir: Direction) {
        if self.direction.is_opposing_to(&dir) {
            println!("Not changing to opposing direction.");
            return;
        }
        let mut rotation = proto::messages::PlayerRotate::new();
        rotation.set_direction(dir.to_proto());
        self.messenger.send_message(proto::messages::MessageType::ROTATE, &rotation);
    }

    #[allow(unused_variables)]
    fn draw_lobby(&self, renderer: &mut sdl2::render::Renderer) {
        renderer.set_draw_color(BACKGROUND_COLOR);
        renderer.clear();

        let surface = self.font.render(&(String::from("Enter your name and press Enter:") + &self.name))
            .blended(Color::RGBA(0, 0, 0, 255))
            .unwrap();
        let mut texture = renderer.create_texture_from_surface(&surface).unwrap();
        let TextureQuery { width, height, .. } = texture.query();
        let target = get_centered_rect(width, height, SCREEN_WIDTH, SCREEN_HEIGHT);

        renderer.copy(&mut texture, None, Some(target));
        renderer.present();
    }

    #[allow(unused_variables)]
    fn draw_playing(&self, renderer: &mut sdl2::render::Renderer) {
        renderer.set_draw_color(BACKGROUND_COLOR);
        renderer.clear();
        renderer.set_draw_color(EMPTY_KOTTEKE_COLOR);
        let player_x: i16 = (self.coords.0 as f64 * KOTTEKE_WIDTH as f64 - self.camera_location.0*KOTTEKE_WIDTH as f64
            + (SCREEN_WIDTH/2) as f64) as i16;
        let player_y: i16 = (self.coords.1 as f64 * KOTTEKE_HEIGHT as f64 - self.camera_location.1*KOTTEKE_HEIGHT as f64
            + (SCREEN_HEIGHT/2) as f64) as i16;

        // Calculate visible range for current camera position
        let start_x: i32 = ((self.camera_location.0*KOTTEKE_WIDTH as f64 - (SCREEN_WIDTH/2) as f64)/KOTTEKE_WIDTH as f64) as i32;
        let start_x = if start_x > 0 { start_x } else { 0 };
        let start_y: i32 = ((self.camera_location.1*KOTTEKE_HEIGHT as f64 - (SCREEN_HEIGHT/2) as f64)/KOTTEKE_HEIGHT as f64) as i32;
        let start_y = if start_y > 0 { start_y } else { 0 };
        let end_x = start_x + (SCREEN_WIDTH/KOTTEKE_WIDTH) as i32;
        let end_x = if end_x < self.field.get_width() as i32 { end_x } else { self.field.get_width() as i32};
        let end_y = start_y + (SCREEN_HEIGHT/KOTTEKE_HEIGHT) as i32;
        let end_y = if end_y < self.field.get_height() as i32 { end_y } else { self.field.get_height() as i32};

        //Draw cells
        for y in start_y-1..end_y + 2 as i32 {
            for x in start_x-1..end_x + 2 as i32 {
                if let Some(cell_addr) =  self.field.get_cell(x, y) {
                    //Draw cell
                    match cell_addr.get_territory() {
                        Some(player) => {
                            //This is where we look up the player in the player db
                            //let c = player.get_color();
                            //renderer.set_draw_color(Color::RGBA(c.0, c.1, c.2, 255));
                            renderer.set_draw_color(Color::RGBA(223, 54 ,0, 127));
                        },
                        None => {
                            renderer.set_draw_color(EMPTY_KOTTEKE_COLOR);
                        },
                    };
                    // Calculate screen coordinates for this cell.
                    let pos_x = x*KOTTEKE_WIDTH as i32 - (self.camera_location.0*KOTTEKE_WIDTH as f64 - SCREEN_WIDTH as f64/2.) as i32
                        - KOTTEKE_WIDTH as i32/2;
                    let pos_y = y*KOTTEKE_HEIGHT as i32 - (self.camera_location.1*KOTTEKE_HEIGHT as f64 - SCREEN_HEIGHT as f64/2.) as i32
                        - KOTTEKE_HEIGHT as i32/2;
                    let kotteke: Rect = Rect::new(pos_x, pos_y,
                                                  KOTTEKE_WIDTH - 1, KOTTEKE_HEIGHT - 1);
                    renderer.fill_rect(kotteke).unwrap();

                    //Draw Tail
                    match cell_addr.get_tail() {
                        Some(player) => {
                            //This is where we look up the player in the player db
                            //let c = player.get_color();
                            //renderer.set_draw_color(Color::RGBA(c.0, c.1, c.2, 255));
                            println!("It has a tail!");
                            renderer.set_draw_color(Color::RGBA(223, 54 ,0, 80));
                            let tail_rect: Rect = Rect::new((pos_x as u32 + 2) as i32,
                                                            (pos_y as u32 + 2) as i32,
                                                            KOTTEKE_WIDTH - 4, KOTTEKE_HEIGHT -4);
                            renderer.fill_rect(tail_rect).unwrap();
                        }
                        None => {}
                    }
                }
            }
        }
        //Draw player
        let player_color = Color::RGBA(223, 54, 0, 255);
        renderer.filled_circle(player_x, player_y, PLAYER_RAD as i16, player_color).unwrap();
        renderer.pixel(player_x, player_y, Color::RGBA(0, 0, 0, 255)).unwrap();

        renderer.present();
    }
}

fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let window = video_subsystem.window("Xilps", SCREEN_WIDTH, SCREEN_HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let mut renderer = window.renderer().build().unwrap();
    renderer.set_draw_color(Color::RGB(255, 255, 255));
    renderer.clear();
    renderer.present();
    let mut event_pump = sdl_context.event_pump().unwrap();

    let addr = "127.0.0.1:13265".parse().unwrap();

    let poll = Poll::new().unwrap();

    let sock = TcpStream::connect(&addr).unwrap();

    // Create storage for events
    let mut events = Events::with_capacity(1024);

    let mut game_client = GameClient::new(sock, &poll);

    poll.register(&game_client.messenger, SERVER, Ready::readable() | Ready::hup(),
                  PollOpt::edge()).unwrap();

    'running: loop {
        // Wait a maximum of 15 milliseconds, after which we draw the next frame.
        poll.poll(&mut events, Some(Duration::from_millis(15))).unwrap();
        for event in events.iter() {
            game_client.event(&poll, event);
        }
        for event in event_pump.poll_iter() {
            match event {
                sdl2::event::Event::Quit {..} | sdl2::event::Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running
                },
                sdl2::event::Event::KeyUp {keycode: Some(keycode), ..} => {
                    game_client.key_down(keycode);
                },
                sdl2::event::Event::TextInput {text, ..} => {
                    println!("TextInput: {}", text);
                    game_client.text_input(text);
                },
                a => {
                    println!("{:?}", a);
                }
            }
        }
        game_client.draw(&mut renderer);
    }
}
