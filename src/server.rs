extern crate mio;
extern crate byteorder;
extern crate protobuf;
extern crate netbuf;
extern crate rand;
extern crate time;
extern crate sdl2; // TODO: conditional compilation. This is here
                   //       for player::Direction::from_sdl(...)

use std::time::Duration;

use mio::*;
use mio::tcp::TcpListener;

pub mod message;
mod game;
mod player;
pub mod proto;
pub mod field;
pub mod contour;

use game::Game;
use player::Player;

const SERVER: Token = Token(0);
const TIMER: Token = Token(1);

const SPEED: u64 = 300;

struct GameServer {
    server: TcpListener,
    game: Game,
    timer: mio::timer::Timer<&'static str>,
}

impl GameServer {
    fn new(poll: &Poll) -> GameServer {
        // Create a socket
        let addr = "127.0.0.1:13265".parse().unwrap();
        let server = TcpListener::bind(&addr).unwrap();
        poll.register(&server, SERVER, Ready::readable(),
                      PollOpt::edge()).unwrap();

        // Create a timer
        let mut timer = mio::timer::Builder::default()
            .tick_duration(Duration::from_millis(SPEED))
            .build();
        timer.set_timeout(Duration::from_millis(SPEED), "UPDATE").unwrap();
        poll.register(&timer, TIMER, Ready::all(),
                      PollOpt::edge()).unwrap();

        let mut gs = GameServer {
            server: server,
            game: Game::new(),
            timer: timer,
        };

        gs.game.initialize_gametime();

        gs
    }
    fn event(&mut self, event_loop: &Poll, event: Event) {
        match event.token() {
            SERVER => {
                let (socket, _) = match self.server.accept() {
                    Err(e) => {
                        println!("Accept error: {}", e);
                        return;
                    }
                    Ok((sock, addr)) => (sock, addr)
                };

                let player = Player::new(socket);
                let new_token = self.game.register_client(player);
                event_loop.register(&self.game.get_player(&new_token).messenger, new_token,
                                    Ready::readable() | Ready::hup(),
                                    PollOpt::edge() | PollOpt::oneshot()).unwrap();
            }
            TIMER => {
                self.timer.set_timeout(Duration::from_millis(SPEED), "UPDATE").unwrap();
                self.game.tick();
            }
            // For all other cases, it's a client:
            token => {
                if event.kind().is_hup() {
                    self.game.remove_player(&token);
                    println!("HUP");
                    return;
                }

                println!("Client {} has something to say", token.0);
                self.game.run_client(&token);
                let client = self.game.get_mut_player(&token).unwrap();
                event_loop.reregister(&client.messenger, token,
                                      Ready::readable() | Ready::hup(),
                                      PollOpt::edge() | PollOpt::oneshot()).unwrap();
            }
        }
    }
}

fn main() {
    let poll = Poll::new().unwrap();

    // Create storage for events
    let mut events = Events::with_capacity(1024);

    let mut game_server = GameServer::new(&poll);

    loop {
        poll.poll(&mut events, None).unwrap();
        for event in events.iter() {
            game_server.event(&poll, event);
        }
    }
}
