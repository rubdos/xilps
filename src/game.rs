use time;
use time::{Timespec, Duration};

use rand;
use rand::Rng;

use std::collections::HashMap;
use mio::Token;

use field::Field;
use player::Player;

use proto::messages::MessageType;


struct GameTime {
    tick_time: Duration,
    last_time: Timespec,
    current_time: Timespec,
    time_diff_sum: Duration,
    total_time: Duration,
}

impl GameTime {
    pub fn new(tick_time: Duration) -> GameTime {
        GameTime {
            tick_time: tick_time,
            last_time: Timespec::new(0, 0),
            current_time: Timespec::new(0, 0),
            time_diff_sum: Duration::zero(),
            total_time: Duration::zero(),
        }
    }

    pub fn initialize(&mut self) {
        self.current_time = time::get_time();
        self.last_time = self.current_time;
    }

    pub fn update(&mut self) -> bool {
        self.current_time = time::get_time();
        self.time_diff_sum = self.time_diff_sum + (self.current_time - self.last_time);
        self.total_time = self.total_time + (self.current_time - self.last_time);
        self.last_time = self.current_time;
        if self.time_diff_sum > self.tick_time {
            self.time_diff_sum = self.time_diff_sum - self.tick_time;
            true
        } else {
            false
        }
    }
}

pub struct Game {
    clients: HashMap<Token, Player>,
    token_counter: usize,
    field: Field,
    gametime: GameTime,
}

impl Game {
    pub fn new() -> Game {
        Game {
            clients: HashMap::new(),
            field: Field::new(100,100),
            gametime: GameTime::new(Duration::milliseconds(300)),
            token_counter: 100,
        }
    }

    pub fn get_player(&mut self, token: &Token) -> &Player {
        &self.clients[token]
    }

    pub fn run_client(&mut self, token: &Token) {
        let was_dead = {
            let mut client = self.clients.get_mut(token).unwrap();
            let was_dead = !client.is_alive();
            client.run();
            was_dead
        };
        if was_dead == self.clients[token].is_alive() {
            // If the client just became awake, we need to give him his initial
            // position and then claim his initial territory.

            // We use a border of 10 around the field
            let new_player = {
                let mut client = self.clients.get_mut(token).unwrap();
                client.coords = (rand::thread_rng().gen_range(10, self.field.get_width() - 10),
                      rand::thread_rng().gen_range(10, self.field.get_height() - 10));

                // Initial territory
                let bottom = client.coords.1 - 2;
                let top    = client.coords.1 + 2;
                let left   = client.coords.0 - 2;
                let right  = client.coords.0 + 2;
                for y  in bottom..top+1 {
                    for x in left..right+1 {
                        self.field.get_mut_cell(x as i32, y as i32).unwrap().territory = Some(token.0);
                    }
                }

                client.serialize_self_as_new_player()
            };

            // We also have to inform all other players about this newly resurrected guy
            for (other_token, other_client) in self.clients.iter_mut() {
                if other_token == token {
                    continue;
                }
                other_client.messenger.send_message(MessageType::NEW_PLAYER, &new_player);
            }
        }
    }

    pub fn get_mut_player(&mut self, token: &Token) -> Option<&mut Player> {
        self.clients.get_mut(&token)
    }

    #[allow(dead_code)]
    pub fn get_map_height(&self) -> usize {
        self.field.get_height()
    }

    #[allow(dead_code)]
    pub fn get_map_width(&self) -> usize {
        self.field.get_width()
    }

    pub fn register_client(&mut self, mut player: Player) -> Token {
        let new_token = Token(self.token_counter);
        player.set_id(new_token.0);

        // Send all players to this player
        for (_, client) in &self.clients {
            if !client.is_alive() {
                continue;
            }
            let client = client.serialize_self_as_new_player();
            player.messenger.send_message(MessageType::NEW_PLAYER, &client);
        }

        self.clients.insert(new_token, player);
        self.token_counter += 1;
        new_token
    }

    pub fn remove_player(&mut self, token: &Token) {
        self.clients.remove(token);
    }

    pub fn initialize_gametime(&mut self) {
        self.gametime.initialize();
    }

    #[allow(dead_code)]
    #[allow(unused_mut)]
    #[allow(unused_variables)]
    fn claim_territory(&mut self, token: &Token) {
        let mut player = self.clients.get_mut(token).unwrap();
        //Primarily sorted on x, secondarily sorted on y
        //This means that later we will need to loop vertically
        //instead of horizontally, because all x-values are grouped.
        let tail = player.get_tail().sort();
        let en_counter = 0;
    }

    fn update_players(&mut self) {
        let mut ids_to_remove: Vec<usize> = Vec::new();
        let mut id_death_row: Vec<usize> = Vec::new();
        let mut territory_shifts: Vec<usize> = Vec::new();
        //Check for collisions
        for player in self.clients.values() {
            if player.is_alive() {
                let (x, y) = player.get_coords();
                match self.field.get_cell(x as i32, y as i32).unwrap().get_tail() {
                    Some(player_id) => {
                        id_death_row.push(player_id);
                    },
                    None => {
                        self.field.get_cell(x as i32, y as i32).unwrap().set_head(player.get_id());
                    },
                }
            }
        }
        //Execution
        for player_id in id_death_row {
            let mut player = self.clients.get_mut(&Token(player_id)).unwrap();
            player.die();
        }
        //Move the cells
        for mut player in self.clients.values_mut() {
            if player.is_alive() {
                player.update_coords(self.field.get_width(), self.field.get_height());
                let (x, y) = player.get_coords();
                let mut cell = self.field.get_cell(x as i32, y as i32).unwrap();
                //Add the tail
                cell.set_tail(player.get_id());
                //Check territory
                let belongs_to: usize = match cell.get_territory() {
                    Some(id) => id,
                    None => 0
                };
                if player.went_outside() && player.get_id() == belongs_to {
                    territory_shifts.push(player.get_id());
                } else if player.get_id() != belongs_to {
                    player.go_outside();
                }
            } else {
                ids_to_remove.push(player.get_id());
            }
        }
        for id in territory_shifts {
            self.claim_territory(&Token(id));
        }
//        for id in ids_to_remove {
//            self.remove_player(&Token(id));
//        }
    }

    pub fn tick(&mut self) {
        self.gametime.update();
        self.update_players();

        // Send everyone the new situation.
        let encoded = self.field.encode();

        // First, encode players
        let encoded_players = {
            let mut encoded_players = vec![];
            for player in self.clients.values() {
                encoded_players.push((player.get_id(), player.serialize_self()));
            }
            encoded_players
        };
        for mut player in self.clients.values_mut() {
            player.send_self();
            player.messenger.send_message(MessageType::BLOCKUPDATE, &encoded);
            // Also, send all players new positions.
            for &(id, ref encoded_player) in encoded_players.iter() {
                if id == player.get_id() {
                    continue; // Shouldn't send ourselves anymore.
                    // TODO: investigate if we can just merge those two. Probably possible.
                }
                player.messenger.send_message(MessageType::PLAYER_STATUS, encoded_player);
            }
        }
    }
}
