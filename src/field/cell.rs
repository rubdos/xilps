#[derive(Copy,Clone)]
#[derive(PartialEq,Eq)]
pub struct Cell {
    pub territory: Option<usize>,
    head: Option<usize>,
    tail: Option<usize>,
}

#[allow(dead_code)]
impl Cell {
    pub fn new() -> Cell {
        Cell {
            territory: None,
            head: None,
            tail: None,
        }
    }

    pub fn get_territory(&self) -> Option<usize> {
        self.territory
    }
    pub fn set_territory(&mut self, player_id: usize) {
        self.territory = Some(player_id);
    }
    pub fn reset_territory(&mut self) {
        self.territory = None;
    }

    pub fn get_head(&self) -> Option<usize> {
        self.head
    }
    pub fn set_head(&mut self, player_id: usize) {
        self.head = Some(player_id);
    }
    pub fn reset_head(&mut self) {
        self.head = None;
    }

    pub fn get_tail(&self) -> Option<usize> {
        self.tail
    }
    pub fn set_tail(&mut self, player_id: usize) {
        self.tail = Some(player_id);
    }
    pub fn reset_tail(&mut self)
    {
        self.tail = None;
    }
}
