use std;
use std::ops::IndexMut;

use player::Direction;
use field::Field;

pub struct EdgeField {
    edges: Vec<Option<Edge>>,
    width: usize,
    height: usize,
}

#[derive(Clone)]
pub struct Edge {
    pub direction: Direction, // tells which direction is inward the player domain.
    pub player_id: usize,
}

impl EdgeField {
    pub fn get_height(&self) -> usize {
        self.height
    }
    pub fn get_width(&self) -> usize {
        self.width
    }
    pub fn at(&self, x: usize, y: usize) -> Option<&Edge> {
        self.edges[y*self.get_width()+x].as_ref()
    }
    pub fn index_mut(&mut self, x: usize, y: usize) -> &mut Option<Edge> {
        let index = y*self.get_width() + x;
        self.edges.index_mut(index)
    }
    pub fn edge_detect(field: &Field) -> EdgeField {
        let mut edges = std::iter::repeat(None).take(field.cells.len()).collect::<Vec<Option<Edge>>>();

        for y in 0..field.get_height() {
            // Horizontal edge detection
            let mut previous = None;
            for x in 0..field.get_height() {
                let territory = field.get_cell(x as i32, y as i32).unwrap().territory;
                // Vertical edge detection
                if y > 0 {
                    let up = field.get_cell(x as i32, (y - 1) as i32).unwrap().territory;
                    if territory.is_some() != up.is_some() { // Non player border
                        // Edge
                        match territory {
                            None => {
                                // Up
                                let player = up.unwrap();
                                edges[(y-1) * field.get_width() + x] = Some(Edge {
                                    player_id: player,
                                    direction: Direction::Up,
                                });
                            },
                            Some(player) => {
                                // Down
                                edges[y * field.get_width() + x] = Some(Edge {
                                    player_id: player,
                                    direction: Direction::Down,
                                });
                            }
                        }
                    }
                    if let (Some(new_id), Some(up_id)) = (territory, up) {
                        if new_id != up_id {
                            // Player border. Do border control
                            edges[(y-1) * field.get_width() + x] = Some(Edge {
                                player_id: up_id,
                                direction: Direction::Up,
                            });
                            edges[y * field.get_width() + x] = Some(Edge {
                                player_id: new_id,
                                direction: Direction::Down,
                            });
                        }
                    }
                } else if y == 0 {
                    if let Some(player) = territory { // Player against upper border
                        // Down
                        edges[y * field.get_width() + x] = Some(Edge {
                            player_id: player,
                            direction: Direction::Down,
                        });
                    }
                } else if y == field.get_height() - 1 {
                    if let Some(player) = territory { // Player against upper border
                        // Up
                        edges[y * field.get_width() + x] = Some(Edge {
                            player_id: player,
                            direction: Direction::Up,
                        });
                    }
                }

                // Horizontal edge detection
                match territory {
                    None => {
                        match previous {
                            None => {},
                            Some(id) => {
                                // From right to left, new edge.
                                edges[y*field.get_width() + x - 1] = Some(Edge {
                                    direction: Direction::Left,
                                    player_id: id,
                                });
                            }
                        }
                    }
                    Some(id) => {
                        match previous {
                            None => {
                                // From left to right, new edge
                                edges[y*field.get_width() + x] = Some(Edge {
                                    direction: Direction::Right,
                                    player_id: id,
                                });
                            }
                            Some(prev_id) if prev_id == id => { // Same id, no op
                            }
                            Some(previous_id) => { // Other guy, two edges
                                edges[y*field.get_width() + x - 1] = Some(Edge {
                                    direction: Direction::Left,
                                    player_id: previous_id,
                                });
                                edges[y*field.get_width() + x] = Some(Edge {
                                    direction: Direction::Right,
                                    player_id: id,
                                });
                            }
                        }
                        if x == field.get_width() - 1 {
                            edges[y*field.get_width() + x] = Some(Edge {
                                direction: Direction::Left,
                                player_id: id,
                            });
                        }
                    }
                }
                previous = territory;
            }
        }
        EdgeField {
            edges: edges,
            width: field.get_width(),
            height: field.get_height(),
        }
    }
}
