use std;
use std::ops::IndexMut;
use std::collections::HashMap;

use player::Direction;

use proto;

use protobuf;

mod cell;
pub use field::cell::Cell;

pub mod edge;
use field::edge::{Edge, EdgeField};

use contour::{Contour, ContourDirection};

pub const FIELD_WIDTH:  usize = 100;
pub const FIELD_HEIGHT: usize = 100;


pub struct Field {
    height: usize,
    width: usize,
    cells: Vec<Cell>,
}

impl Field {
    pub fn new(width: usize, height: usize) -> Field {
        let cells = std::iter::repeat(Cell::new())
                .take(height*width)
                .collect::<Vec<_>>();

        Field {
            height: height,
            width: width,
            cells: cells
        }
    }
    pub fn get_cell(&self, x: i32, y: i32) -> Option<Cell> {
        let usize_x: usize = x as usize;
        let usize_y: usize = y as usize;
        if (0 <= x) && (usize_x < self.width) && (0 <= y) && (usize_y < self.height) {
            Some(self.cells[usize_x + usize_y * self.width])
        }
        else {
            None
        }
    }

    pub fn get_mut_cell(&mut self, x: i32, y: i32) -> Option<&mut Cell> {
        let usize_x: usize = x as usize;
        let usize_y: usize = y as usize;
        if (0 <= x) && (usize_x < self.width) && (0 <= y) && (usize_y < self.height) {
            Some(self.cells.index_mut(usize_x + usize_y * self.width))
        }
        else {
            None
        }
    }

    pub fn get_height(&self) -> usize {
        self.height
    }
    pub fn get_width(&self) -> usize {
        self.width
    }

    pub fn merge(&mut self, update: &proto::messages::BlockUpdate) {
        if update.get_keyframe() {
            // Reset the whole field
            self.cells = std::iter::repeat(Cell::new())
                    .take(self.get_height() * self.get_width())
                    .collect::<Vec<_>>();
        }
        for fieldspec in update.get_fields() {
            let player_id = fieldspec.get_player_id() as usize;

            let pos = fieldspec.get_begin();
            let (mut x, mut y) = (pos.get_x()as usize, pos.get_y() as usize);

            self.get_mut_cell(x as i32, y as i32).unwrap().territory = Some(player_id);

            let contour = Contour::decode(Vec::from(fieldspec.get_coded()));

            let mut matcher = vec![None; self.get_height()];
            if contour.len() > 0 {
                if contour[0] == ContourDirection::Down
                    || contour[0] == ContourDirection::DownLeft
                    || contour[0] == ContourDirection::DownRight {
                    matcher[y] = Some(x);
                }
            }

            let mut pop_matrix: HashMap<(usize, usize/*x,y*/), usize /*player*/> = HashMap::new();
            for item in contour[..contour.len()].iter() {
                let (x_prev, y_prev) = (x, y);
                let newpos = item.next(x, y); // This gets a lot nicer with
                x = newpos.0;                 // https://github.com/rust-lang/rfcs/issues/372
                y = newpos.1;
                // Assign to player (because border is included in coding).
                self.get_mut_cell(x as i32, y as i32).unwrap().territory = Some(player_id);

                // If this edge goes down, register edge
                // if this edge goes up, search matching horizontal partner and fill as needed
                match *item {
                    ContourDirection::Down |
                    ContourDirection::DownLeft |
                    ContourDirection::DownRight => {
                        matcher[y] = Some(x);
                        matcher[y_prev] = Some(x_prev);
                    }
                    ContourDirection::Up |
                    ContourDirection::UpRight |
                    ContourDirection::UpLeft => {
                        match matcher[y] {
                            None => unreachable!(), // Let's hope
                            Some(match_x) if match_x > x => {
                                // Match is to the right. Set all empty
                                for i in (x + 1)..match_x {
                                    let key = (i,y);
                                    self.get_mut_cell(i as i32, y as i32).unwrap().territory = match pop_matrix.get(&key) {
                                        Some(player) => Some(*player),
                                        None => None
                                    };
                                }
                            }
                            Some(match_x) if match_x < x => {
                                // Match is to the left, get rid of already filled cells.
                                for i in match_x..(x + 1) {
                                    let key = (i,y);
                                    match self.get_cell(i as i32, y as i32).unwrap().territory {
                                        Some(player_id) => {
                                            if pop_matrix.get(&key).is_none() {
                                                pop_matrix.insert(key, player_id);
                                            };
                                        }
                                        None => {}
                                    };
                                    self.get_mut_cell(i as i32, y as i32).unwrap().territory = Some(player_id);
                                }
                            }
                            Some(match_x) if match_x == x => {
                                // Nop
                            }
                            Some(_) => unreachable!() // The same
                        };
                    }
                    _ => {}
                }
            }
        }
    }

    pub fn encode(&self) -> proto::messages::BlockUpdate {
        let mut update = proto::messages::BlockUpdate::new();
        update.set_keyframe(true);

        let edges = EdgeField::edge_detect(self);
        let contours = Contour::get_contours(edges);

        let mut fieldspecs = vec![];

        for contour in contours {
            let mut fieldspec = proto::messages::FieldSpec::new();
            fieldspec.set_player_id(contour.player_id as u32);
            let mut begin = proto::messages::Coords::new();
            begin.set_x(contour.contour[0].0 as u32);
            begin.set_y(contour.contour[0].1 as u32);
            fieldspec.set_begin(begin);
            let contour = contour.encode();
            fieldspec.set_coded(contour);
            fieldspecs.push(fieldspec);
        }

        // In the rust-protobuf test sources, they do this cleanly. Have to find out what I'm
        // missing here.
        update.set_fields(protobuf::repeated::RepeatedField::from_vec(fieldspecs));

        update
    }
}

// ----- UNIT TESTS ------

#[test]
fn test_get_cell() {
    let field_width: i32= 10;
    let field_height: i32 = 8;
    let f = Field::new(field_width as usize, field_height as usize);
    //Out of bounds
    assert!(f.get_cell(field_width, field_height).is_none());
    assert!(f.get_cell(-1, -1).is_none());
    //Corner cases
    assert!(f.get_cell(field_width - 1, field_height - 1).is_some());
    assert!(f.get_cell(0,0).is_some());
}

use std::fmt::{Formatter, Debug, Result};

impl PartialEq for Field {
    fn eq(&self, other: &Field) -> bool {
        if other.get_width() != self.get_width() {
            return false;
        }
        if other.get_height() != self.get_height() {
            return false;
        }
        for y in 0..self.get_height() {
            for x in 0..self.get_width() {
                if self.get_cell(x as i32, y as i32) != other.get_cell(x as i32, y as i32) {
                    return false;
                }
            }
        }
        true
    }
}
impl Eq for Field {
}
impl Debug for Field {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "Some ({} x {}) Field", self.get_width(), self.get_height())
    }
}

#[test]
fn field_encoding() {
    let mut f = Field::new(50, 50);
    for y in 23..28 {
        for x in 23..28 {
            f.get_mut_cell(x, y).unwrap().territory = Some(10);
        }
    }

    let mut target = Field::new(50, 50);
    target.merge(&f.encode());
    assert_eq!(f, target);
}

#[test]
fn field_encoding_edge_1() {
    // AAAAA
    // AAAAA
    // AABAA <- This B creates two extra, extra difficult, contours.
    // AAAAA
    // AAAAA
    let mut f = Field::new(50, 50);
    for y in 23..28 {
        for x in 23..28 {
            f.get_mut_cell(x , y).unwrap().territory = Some(10);
        }
    }

    f.get_mut_cell(25, 25).unwrap().territory = Some(11);

    let mut target = Field::new(50, 50);
    target.merge(&f.encode());
    assert_eq!(f, target);
}

#[test]
fn field_encoding_edge_2() {
    // A   A
    // A   A <- single width stuff. Could be cumbersome
    // AAAAA
    // AAAAA
    // AAAAA
    let mut f = Field::new(50, 50);
    for y in 23..25 {
        for x in [23, 28].iter() {
            f.get_mut_cell(*x, y).unwrap().territory = Some(10);
        }
    }
    for y in 25..28 {
        for x in 23..28 {
            f.get_mut_cell(x, y).unwrap().territory = Some(10);
        }
    }

    let mut target = Field::new(50, 50);
    target.merge(&f.encode());
    assert_eq!(f, target);
}

#[test]
fn field_encoding_edge_3() {
    // AAAAA
    // AAAAA
    // BBABB <- This A can give trouble in decoding.
    // BBBBB
    // BBBBB
    let mut f = Field::new(50, 50);
    for y in 23..25 {
        for x in 23..28 {
            f.get_mut_cell(x, y).unwrap().territory = Some(10);
        }
    }
    for y in 25..28 {
        for x in 23..28 {
            f.get_mut_cell(x, y).unwrap().territory = Some(11);
        }
    }
    f.get_mut_cell(25, 25).unwrap().territory = Some(10);

    let mut target = Field::new(50, 50);
    target.merge(&f.encode());
    assert_eq!(f, target);
}

#[test]
fn field_encoding_edge_4() {
    // CCCCC
    // CAAAC
    // CABAC <- This B creates two extra, extra difficult, contours.
    // CAAAC <- C will probably make it even more difficult
    // CCCCC
    let mut f = Field::new(50, 50);
    for y in 23..28 {
        for x in 23..28 {
            f.get_mut_cell(x, y).unwrap().territory = Some(10);
        }
    }
    for y in 24..27 {
        for x in 24..27 {
            f.get_mut_cell(x, y).unwrap().territory = Some(11);
        }
    }
    f.get_mut_cell(25, 25).unwrap().territory = Some(12);

    let mut target = Field::new(50, 50);
    target.merge(&f.encode());
    assert_eq!(f, target);
}
