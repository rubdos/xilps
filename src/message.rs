use std::io::{Error, Read, ErrorKind, Write};
use std::collections::vec_deque::VecDeque;

use mio::{Evented, Poll, PollOpt, Token, Ready};
use mio::tcp::TcpStream;
use netbuf::Buf;

use byteorder::{BigEndian, ByteOrder, WriteBytesExt};

use protobuf;
use protobuf::{Message, MessageStatic};
use proto::messages::{BaseMessage, MessageType};

pub struct BufferedMessageQueue {
    current_message_length: u32,
    sock: TcpStream,
    read_continuation: Option<u32>,
    current_message: Option<Buf>,
    messages: VecDeque<BaseMessage>,
}

impl BufferedMessageQueue {
    pub fn new(sock: TcpStream) -> Self {
        BufferedMessageQueue {
            current_message_length: 0,
            sock: sock,
            read_continuation: None,
            current_message: None,
            messages: VecDeque::new(),
        }
    }

    pub fn next_message_type(&self) -> Option<MessageType> {
        match self.messages.front() {
            Some(msg) => Some(msg.get_message_type()),
            None => None,
        }
    }

    pub fn pop_message<T: Message+MessageStatic>(&mut self) -> Option<T> {
        match self.messages.pop_front() {
            Some(msg) => Some(protobuf::parse_from_bytes::<T>(msg.get_contents()).unwrap()),
            None => None,
        }
    }

    pub fn send_message(&mut self, message_type: MessageType, msg: &Message) {
        let mut temp = vec![];
        msg.write_to_writer(&mut temp).unwrap();

        let mut base = BaseMessage::new();
        base.set_contents(temp);
        base.set_message_type(message_type);

        temp = vec![];
        base.write_to_writer(&mut temp).unwrap();
        let len = temp.len();

        self.sock.write_u32::<BigEndian>(len as u32).unwrap();
        self.sock.write_all(&mut temp).unwrap();
        self.sock.flush().unwrap();
    }

    fn read_message_length(&mut self) -> Result<Option<u32>, Error> {
        if let Some(n) = self.read_continuation {
            return Ok(Some(n));
        }

        let mut buf = [0u8; 4];

        let bytes = match self.sock.read(&mut buf) {
            Ok(n) => n,
            Err(e) => {
                if e.raw_os_error().unwrap() == 11 {
                    return Ok(None);
                } else {
                    return Err(e);
                }
            }
        };

        if bytes < 4 {
            println!("Found invalid message length header of {} bytes", bytes);
            return Err(Error::new(ErrorKind::Other, "Invalid message length"));
        }
        if bytes > 4 {
            panic!("Buffer overflow?");
        }

        let msg_len = BigEndian::read_u32(buf.as_ref());
        Ok(Some(msg_len))
    }

    pub fn run(&mut self) {
        loop {
            match self.read_message_length() {
                Ok(None) => {
                    return;
                }
                Ok(Some(n)) => {
                    self.current_message_length = n;
                    if self.read_continuation == None {
                        self.read_continuation = Some(n);
                        self.current_message = Some(Buf::new());
                    }

                    let bytes: usize = match self.current_message.as_mut().unwrap().read_max_from(n as usize, &mut self.sock) {
                        Ok(max_reached) => {
                            let bytes = self.current_message.as_ref().unwrap().len();
                            self.read_continuation = Some(self.read_continuation.unwrap() - n as u32);
                            if max_reached {
                                // Message complete, move it on the queue
                                self.read_continuation = None;
                                let mut contents: Vec<u8> = self.current_message.take().unwrap().into();
                                let msg = protobuf::parse_from_bytes::<BaseMessage>(&mut contents).unwrap();
                                self.messages.push_back(msg);
                            }
                            bytes
                        },
                        Err(e) => {
                            if e.raw_os_error().unwrap() == 11 {
                                println!("Buffer underrun");
                                0
                            } else {
                                panic!("Something wrong! {}", e);
                            }
                        }
                    };
                    // Note: this needs testing, but it should work(tm).
                    if bytes < n as usize {
                        println!("Not enough bytes received. Implement more caching");
                        return;
                    }

                    // Note: this needs testing, but won't work yet.
                    if bytes > n as usize {
                        panic!("Read to many bytes, need to implement overflow");
                        return;
                    }
                }
                _ => unreachable!("Invalid messages not implemented") // Possibly disconnect
            }
        }
    }
}

impl Evented for BufferedMessageQueue {
    fn register(&self, poll: &Poll, token: Token,
                interest: Ready, opts: PollOpt) -> Result<(), Error> {
        // And register the new client's socket to the EventLoop
        poll.register(&self.sock, token, interest,
                      opts).unwrap();
        Ok(())
    }

    fn reregister(&self, poll: &Poll, token: Token,
                  interest: Ready, opts: PollOpt) -> Result<(), Error> {
        poll.reregister(&self.sock, token, interest,
                        opts).unwrap();
        Ok(())
    }

    #[allow(unused_variables)]
    fn deregister(&self, poll: &Poll) -> Result<(), Error> {
        return Err(Error::new(ErrorKind::Other, "BufferedMessageReader::deregister not implemented"));
    }
}
