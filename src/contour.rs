use std::collections::VecDeque;
use player::Direction;

use field::edge::{Edge, EdgeField};

#[derive(Copy,Clone,PartialEq,Debug)]
pub enum ContourDirection {
    Up,
    UpRight,
    Right,
    DownRight,
    Down,
    DownLeft,
    Left,
    UpLeft,
}

impl ContourDirection {
    pub fn next(&self, x: usize, y: usize) -> (usize, usize) {
        let delta = match *self {
             ContourDirection::Left      => (-1,  0),
             ContourDirection::DownLeft  => (-1,  1),
             ContourDirection::Down      => ( 0,  1),
             ContourDirection::DownRight => ( 1,  1),
             ContourDirection::Right     => ( 1,  0),
             ContourDirection::UpRight   => ( 1, -1),
             ContourDirection::Up        => ( 0, -1),
             ContourDirection::UpLeft    => (-1, -1),
        };
        ((x as i32 + delta.0) as usize, (y as i32 + delta.1) as usize)
    }

    fn from_edge_direction(d: &Direction) -> ContourDirection {
        match *d {
            Direction::Right => ContourDirection::Down,
            Direction::Down => ContourDirection::Left,
            Direction::Left => ContourDirection::Up,
            Direction::Up => ContourDirection::Right,
        }
    }

    fn from_coordinates(r1: (usize, usize), r2: (usize, usize)) -> ContourDirection {
        let diff = (r2.0 as i32 - r1.0 as i32,
                    r2.1 as i32 - r1.1 as i32);
        match diff {
             (-1,  0) => ContourDirection::Left,
             (-1,  1) => ContourDirection::DownLeft,
             ( 0,  1) => ContourDirection::Down,
             ( 1,  1) => ContourDirection::DownRight,
             ( 1,  0) => ContourDirection::Right,
             ( 1, -1) => ContourDirection::UpRight,
             ( 0, -1) => ContourDirection::Up,
             (-1, -1) => ContourDirection::UpLeft,
             (_,   _) => {
                 panic!("delta is not sqrt(2): {} {}", diff.0, diff.1);
                 unreachable!()
             } // Or let's hope
        }
    }

    pub fn simple_encoding(&self) -> Vec<u8> {
        match *self {
            ContourDirection::Up        => vec![0,0,0],
            ContourDirection::UpRight   => vec![0,0,1],
            ContourDirection::Right     => vec![0,1,0],
            ContourDirection::DownRight => vec![0,1,1],
            ContourDirection::Down      => vec![1,0,0],
            ContourDirection::DownLeft  => vec![1,0,1],
            ContourDirection::Left      => vec![1,1,0],
            ContourDirection::UpLeft    => vec![1,1,1],
        }
    }

    pub fn simple_decoding(x:u8,y:u8,z:u8) -> ContourDirection {
        match (x, y, z) {
            (0,0,0) => ContourDirection::Up,
            (0,0,_) => ContourDirection::UpRight,
            (0,_,0) => ContourDirection::Right,
            (0,_,_) => ContourDirection::DownRight,
            (_,0,0) => ContourDirection::Down,
            (_,0,_) => ContourDirection::DownLeft,
            (_,_,0) => ContourDirection::Left,
            (_,_,_) => ContourDirection::UpLeft,
        }
    }
}

//TODO: implement Indexing trait for contour
//      get rid of player_id in here.
pub struct Contour {
    pub player_id: usize,
    // Vector of the coordinates.
    // In case of a field, it forms a closed loop; i.e. the first coordinate equals the
    // last coordinate.
    // In case of a player tail, it doesn't close.
    pub contour: Vec<(usize, usize, Option<ContourDirection>)>,
}

impl Contour {
    pub fn from_player_tail(end: &(usize, usize), list: &Vec<(i32, i32)>) -> Contour {
        if list.len() < 1 {
            return Contour {
                player_id: 0,
                contour: vec![],
            };
        }
        let (mut x, mut y) = (list[0].0 as usize, list[0].1 as usize);
        let list = list.iter().map(|&(x,y)| (x as usize, y as usize));

        let mut contour = vec![];

        for (new_x, new_y) in list.skip(1) {
            if new_x == x && new_y == y {
                // Whatever, not important
                println!("Warning, redundant element in from_player_tail");
                continue;
            }
            contour.push((new_x, new_y, Some(ContourDirection::from_coordinates((x, y), (new_x, new_y)))));
            x = new_x; y = new_y;
        }
        if !(end.0 == x && end.1 == y) {
            contour.push((end.0, end.1, Some(ContourDirection::from_coordinates((x, y), (end.0, end.1)))));
        }

        Contour {
            player_id: 0, // Have to get rid of player_id in contour.
            contour: contour,
        }
    }

    fn find_contour<F>(_x: usize, _y: usize, begin_direction: ContourDirection,
                    mut next_neighbour: F) -> Vec<(usize, usize, Option<ContourDirection>)>
    where F: FnMut(usize, usize, ContourDirection) -> Option<(usize, usize, ContourDirection)> {
        let mut contour = vec![];

        // Some unused edge was found.
        let (mut x, mut y) = (_x,_y);
        let mut current_direction = begin_direction;

        contour.push((x, y, None));


        // Filter on relevant edges
        while let Some(next_edge) = next_neighbour(x, y, current_direction) {
            x = next_edge.0;
            y = next_edge.1;
            current_direction = next_edge.2;
            contour.push((x,y, Some(current_direction)));
            if x == _x && y == _y {
                break;
            }
        }
        contour
    }

    pub fn get_contours(mut edges: EdgeField) -> Vec<Contour> {
        let mut contours = Vec::<_>::new();

        // Search for an unused edge.
        for _y in 0..edges.get_height() as usize{
            for _x in 0..edges.get_width() as usize{
                if edges.at(_x, _y).is_some() {
                    let (player_id, begin_direction) = {
                        let edge = edges.at(_x, _y).unwrap();

                        let Edge {
                            direction: ref start_direction, // tells which direction is inward the player domain.
                            player_id,
                        } = *edge;

                        let begin_direction = ContourDirection::from_edge_direction(start_direction);
                        (player_id, begin_direction)
                    };

                    let contour = {
                        let next_neighbour = |x, y, dir| -> Option<(usize, usize, ContourDirection)> {
                            let deltas = [
                                (-1,  0,  ContourDirection::Left),
                                (-1,  1, ContourDirection::DownLeft),
                                ( 0,  1, ContourDirection::Down),
                                ( 1,  1, ContourDirection::DownRight),
                                ( 1,  0, ContourDirection::Right),
                                ( 1, -1, ContourDirection::UpRight),
                                ( 0, -1, ContourDirection::Up),
                                (-1, -1, ContourDirection::UpLeft)
                            ];
                            // Rotate delta's
                            let deltas = {
                                let mut index: i32 = 0;
                                while !(deltas[index as usize].2 == dir) {
                                    index += 1;
                                }
                                index -= 2; // Rotate back 90 degrees.
                                let mut v = Vec::new();
                                if index < 0 {
                                    index += deltas.len() as i32;
                                }
                                let index = index as usize;
                                v.extend(deltas[index..deltas.len()].iter());
                                v.extend(deltas[0..index].iter());
                                v
                            };
                            deltas.iter().map(|dd| {
                                let (dx, dy, ref newdir) = *dd;
                                if x as i32 + dx < 0
                                    || x as i32 + dx >= (edges.get_width() - 1) as i32
                                    || y as i32 + dy < 0
                                    || y as i32 + dy >= (edges.get_height() - 1) as i32 {
                                    None
                                } else {
                                    Some((x as i32 + dx, y as i32 + dy, newdir))
                                }
                                //&none
                            }).filter(|x| x.is_some())
                                .map(|x| x.unwrap())
                                .map(|(x, y, dir)| (x as usize, y as usize, *dir))
                                .filter(|&(x, y, _)| {
                                    match edges.at(x, y) {
                                        Some(&Edge{
                                            direction: _,
                                            player_id: new_player_id
                                        }) if new_player_id == player_id => true,
                                        _ => false
                                    }
                                }).next()
                        };
                        Contour::find_contour(_x, _y, begin_direction, next_neighbour)
                    };

                    // Now get rid of the edges in the edgegrid
                    for &(x, y, _) in contour.iter() {
                        *edges.index_mut(x, y) = None;
                    }

                    contours.push(Contour {
                        player_id: player_id,
                        contour: contour,
                    });
                }
            }
        }
        contours
    }

    pub fn encode(&self) -> Vec<u8> {
        let mut encoding = vec![];
        let mut current_byte = 0u8;
        let mut progress_byte = 0u8;

        encoding.push(0u8); // Push the last-byte-mask
                            // to be edited after the loop.

        const PROGRESS_FULL_BYTE: u8 = 255u8;

        for &(_, _, contour_direction) in &self.contour {
            if contour_direction.is_none() {
                continue;
            }


            for c in contour_direction.unwrap().simple_encoding() {
                current_byte = (current_byte << 1) | c;
                progress_byte = (progress_byte << 1) | 1;
                if progress_byte == PROGRESS_FULL_BYTE {
                    encoding.push(current_byte);
                    progress_byte = 0u8;
                }
            }
        }

        let mut unused_bitcount = progress_byte.leading_zeros();
        if unused_bitcount < 8 {
            encoding.push(current_byte << unused_bitcount);
        }
        if unused_bitcount == 8 {
            unused_bitcount = 0;
        }
        encoding[0] = unused_bitcount as u8;
        encoding
    }

    pub fn to_coordinates(begin: (usize, usize), encoding: Vec<u8>) -> Vec<(i32, i32)> {
        let directions = Self::decode(encoding);

        let mut coordinates = Vec::with_capacity(directions.len() + 1);
        coordinates.push((begin.0 as i32, begin.1 as i32));

        let (mut x, mut y) = begin;
        for direction in directions {
            let next = direction.next(x, y);
            {
                let x = next.0 as i32;
                let y = next.1 as i32;
                coordinates.push((x, y));
            }
            x = next.0; y = next.1;
        }
        coordinates
    }

    pub fn decode(encoding: Vec<u8>) -> Vec<ContourDirection> {
        let mut decoding = vec![];
        let unused_bitcount = encoding[0];

        if encoding.len() == 1 {
            return decoding;
        }

        let mut bits = VecDeque::with_capacity(8);

        for new_byte in encoding[1..encoding.len()-1].iter()  {
            let mut new_byte = *new_byte;
            for _ in 0..8 {
                bits.push_back(new_byte & 128);
                new_byte = new_byte << 1;
            }
            while bits.len() >= 3 {
                let x = bits.pop_front().unwrap();
                let y = bits.pop_front().unwrap();
                let z = bits.pop_front().unwrap();
                decoding.push(ContourDirection::simple_decoding(x,y,z));
            }
        }
        // Handle last byte separately.
        let mut new_byte = encoding[encoding.len()-1];
        for _ in 0..(8-unused_bitcount) {
            bits.push_back(new_byte & 128);
            new_byte = new_byte << 1;
        }
        while bits.len() >= 3 {
            let x = bits.pop_front().unwrap();
            let y = bits.pop_front().unwrap();
            let z = bits.pop_front().unwrap();
            decoding.push(ContourDirection::simple_decoding(x,y,z));
        }
        assert_eq!(bits.len(), 0);

        decoding
    }
}
