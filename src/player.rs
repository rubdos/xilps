use mio::tcp::TcpStream;
use sdl2::keyboard::Keycode;

use rand;
use rand::Rng;

use message::BufferedMessageQueue;

use proto;
use proto::messages::MessageType;
use proto::messages::{RegisterPlayer, PlayerRotate, PlayerStatus, NewPlayer, Color};

use contour::{Contour, ContourDirection};

#[derive(Clone,Debug,PartialEq)]
pub enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl Direction {
    pub fn random() -> Direction {
        let mut rng = rand::thread_rng();
        match rng.next_u32() % 4 {
            0 => Direction::Up,
            1 => Direction::Right,
            2 => Direction::Down,
            3 => Direction::Left,
            _ => unreachable!()
        }
    }

    pub fn is_opposing_to(&self, other: &Direction) -> bool {
        match *self {
            Direction::Up => {
                if *other == Direction::Down {
                    return true;
                }
            },
            Direction::Right => {
                if *other == Direction::Left {
                    return true;
                }
            }
            Direction::Down  => {
                if *other == Direction::Up {
                    return true;
                }
            }
            Direction::Left  => {
                if *other == Direction::Right {
                    return true;
                }
            }
        }
        false
    }

    pub fn from_sdl(dir: Keycode) -> Direction {
        match dir {
            Keycode::Left => Direction::Left,
            Keycode::Right => Direction::Right,
            Keycode::Up => Direction::Up,
            Keycode::Down => Direction::Down,
            _ => unreachable!()
        }
    }

    pub fn from_proto(dir: proto::messages::Direction) -> Direction {
        match dir {
            proto::messages::Direction::UP => Direction::Up,
            proto::messages::Direction::RIGHT => Direction::Right,
            proto::messages::Direction::DOWN => Direction::Down,
            proto::messages::Direction::LEFT => Direction::Left,
        }
    }

    pub fn to_proto(&self) -> proto::messages::Direction {
        match *self {
            Direction::Up => proto::messages::Direction::UP,
            Direction::Right => proto::messages::Direction::RIGHT,
            Direction::Down => proto::messages::Direction::DOWN,
            Direction::Left => proto::messages::Direction::LEFT,
        }
    }
}

pub struct Player {
    pub messenger: BufferedMessageQueue,
    pub direction: Direction,
    pub name: String,
    id: usize,
    alive: bool,
    pub coords: (usize, usize),
    #[allow(dead_code)]
    color: (u8, u8, u8),
    tail: Vec<(i32, i32)>,
    claiming: bool,
}

impl Player {
    pub fn new(stream: TcpStream) -> Player {
        let c = (rand::thread_rng().gen_range(0, 256) as u8,
                rand::thread_rng().gen_range(0, 256) as u8,
                rand::thread_rng().gen_range(0, 256) as u8);
        Player {
            messenger: BufferedMessageQueue::new(stream),
            direction: Direction::random(),
            name: String::new(),
            id: 0,
            alive: false,
            coords: (0, 0), // Some initial stupid coord
            color: c,
            tail: vec![],
            claiming: false,
        }
    }

    pub fn set_id(&mut self, id: usize) {
        self.id = id;
    }

    pub fn get_id(&self) -> usize {
        self.id
    }

    pub fn run(&mut self) {
        self.messenger.run();

        while let Some(message_type) = self.messenger.next_message_type() {
            match message_type {
                MessageType::REGISTER => {
                    let message = self.messenger.pop_message::<RegisterPlayer>().unwrap();
                    self.name = String::from(message.get_name());
                    self.resurrect();
                    println!("Registered {}", self.name);
                    self.send_self();
                }
                MessageType::ROTATE => {
                    let message = self.messenger.pop_message::<PlayerRotate>().unwrap();
                    self.direction = Direction::from_proto(message.get_direction());
                }
                MessageType::PLAYER_STATUS |
                MessageType::BLOCKUPDATE |
                MessageType::NEW_PLAYER => {
                    //Nobody sends these, for now unreachable, but we should probably catch these.
                    unreachable!();
                }
            }
        }
    }

    pub fn serialize_self(&self) -> proto::messages::PlayerStatus{
        let mut status = PlayerStatus::new();
        status.set_id(self.id as u32);
        status.set_direction(self.direction.to_proto());
        let mut coords = proto::messages::Coords::new();
        coords.set_x(self.coords.0 as u32);
        coords.set_y(self.coords.1 as u32);
        status.set_coords(coords);

        let contour = Contour::from_player_tail(&self.coords, &self.tail);
        if contour.contour.len() > 0 {
            let mut coords = proto::messages::Coords::new();
            coords.set_x(self.tail[0].0 as u32);
            coords.set_y(self.tail[0].1 as u32);
            status.set_tail_begin(coords);
            status.set_tail(contour.encode());
        }

        status
    }

    pub fn serialize_self_as_new_player(&self) -> proto::messages::NewPlayer {
        let status = self.serialize_self();
        let color = {
            let mut color = Color::new();
            color.set_R(self.color.0 as u32);
            color.set_G(self.color.1 as u32);
            color.set_B(self.color.2 as u32);
            color
        };

        {
            let mut newplayer = NewPlayer::new();
            newplayer.set_player_status(status);
            newplayer.set_name(self.name.clone());
            newplayer.set_color(color);
            newplayer
        }
    }

    pub fn send_self(&mut self) {
        let status = self.serialize_self();
        self.messenger.send_message(MessageType::PLAYER_STATUS, &status);
    }

    pub fn die(&mut self) {
        self.alive = false;
    }

    pub fn resurrect(&mut self) {
        self.alive = true;
    }

    pub fn is_alive(&self) -> bool {
        self.alive
    }

    pub fn get_coords(&self) -> (usize, usize) {
        self.coords
    }


    pub fn update_coords(&mut self, width: usize, height: usize) {
        // Perhaps we should implement a named tuple with some traits
        // for coordinates/vectors...
        let c = (self.coords.0 as i32, self.coords.1 as i32);
        let new_coord: (i32, i32) = match self.direction {
            Direction::Up    => ( c.0    , c.1 - 1),
            Direction::Down  => ( c.0    , c.1 + 1),
            Direction::Left  => ( c.0 - 1, c.1    ),
            Direction::Right => ( c.0 + 1, c.1    ),
        };
        if (0 <= new_coord.0) && (new_coord.0  < width as i32) &&
            (0 <= new_coord.1) && (new_coord.1  < height as i32) {
                self.coords = (new_coord.0 as usize, new_coord.1 as usize);
                self.tail.push(new_coord);
                println!("{}, {}", new_coord.0, new_coord.1);
        }
        else {
            println!("A white light!");
            self.die();
        }
    }

    pub fn get_tail(&self) -> Vec<(i32, i32)> {
        self.tail.clone()
    }
    #[allow(dead_code)]
    pub fn flush_tail(&mut self) {
        self.tail.clear();
    }

    pub fn went_outside(&self) -> bool {
        self.claiming
    }
    pub fn go_outside(&mut self) {
        self.claiming = true;
    }

    #[allow(dead_code)]
    pub fn go_inside(&mut self) {
        self.claiming = false;
    }

    #[allow(dead_code)]
    pub fn get_color(&self) -> (u8, u8, u8) {
        self.color
    }
}


// ----- UNIT TESTS ------

#[test]
fn encoding_1() {
    let coords = (12, 13);
    let tail = vec![(10, 10), (10, 11), (11, 11), (12, 12)];
    let encoding = Contour::from_player_tail(&coords, &tail).encode();
    let decoding = Contour::decode(encoding);
    let comparison = vec![
        ContourDirection::Down,
        ContourDirection::Right,
        ContourDirection::DownRight,
        ContourDirection::Down,
    ];
    assert_eq!(decoding, comparison);
}

#[test]
fn encoding_2() {
    let coords = (12, 13);
    let tail = vec![(10, 10), (10, 11), (11, 11), (12, 12)];
    let encoding = Contour::from_player_tail(&coords, &tail).encode();
    let decoded_coords = Contour::to_coordinates((10, 10), encoding);
    for (a, b) in tail.iter().map(|&(x, y)| (x as i32, y as i32)).zip(decoded_coords[..].iter()) {
        assert_eq!(a, *b);
    }
}
